# insubriparks_maps

Repository of web maps developed within the project INSUBRI.PARKS funded by the Interreg Co-operation Programme 2014 -2020 (ID 605472). Maps are published within sections of the online portal https://insubriparksturismo.eu.

Different maps are included in different branches of this repo.

- [Infographic map](https://gitlab.com/geolab.como/insubriaparks_maps/-/tree/MappeSinottiche) 
- [Parks maps](https://gitlab.com/geolab.como/insubriaparks_maps/-/tree/MappeDettaglio-Parchi)
- [Itineraries maps](https://gitlab.com/geolab.como/insubriaparks_maps/-/tree/MappeDettaglio-Sentieri)


## Authors and acknowledgement
[GEOlab](http://www.geolab.polimi.it/) - Politecnico di Milano (contact: [Daniele Oxoli](mailto:daniele.oxoli@polimi.it))

Developed by [Matteo Rizzi](mailto:matteorizzi9300@gmail.com) 

## License
For open source projects, say how it is licensed.


